import React, {Component} from 'react';
import BeerService from "../services/beer.service";


class Detail extends Component {


    constructor(props) {
        super(props);
        this.state = {
            beer: []
        };
    }

    async componentDidMount() {
        let url = window.location.href;
        let id = url.substring(url.lastIndexOf('/') + 1); // Prend la dernière valeur de l'url pour avoir l'id
        let response = await BeerService.DetailsBeers(id);
        if (response.ok) {
            let data = await response.json();
            this.setState({beer: data.beer});

        } else {
            console.log(response.error);
        }
    }

    render(){
        return (
            <div>{console.log(this.state.beer)}
                <div>
                    <p>{this.state.beer.name}</p>
                    <p>{this.state.beer.description}</p>
                    <p>{this.state.beer.ABV}</p>
                    <p>{this.state.beer.EBC}</p>
                    <p>{this.state.beer.image}</p>
                </div>
            </div>
        )
    }


}

export default Detail;