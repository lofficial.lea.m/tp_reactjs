import React, {Component} from 'react';
import Menu from '../pages/Menu';
//import UserService from "../services/user.service";
import BeerService from "../services/beer.service";
import Beer from "../components/Beer";


class Beers extends Component {

    constructor(props) {
        super(props);
        this.state = {
            title: "???",
            beers: []
        }
    }

    async componentDidMount() {
        let response = await BeerService.list();
        console.log(response);
        if (response.ok) {
            let data = await response.json();
            console.log(data);
            this.setState({beers: data.beer});

        } else {
            console.log(response.error);
        }
    }

    async deleteBeer(id) {
        let response = await BeerService.delete(id);
        if (response.ok) {
            let beers = this.state.beers;
            let index = beers.findIndex(post => post._id === id);
            beers.splice(index, 1);
            this.setState({beers: beers});
        }
    }


    render() {
        return (
            <div>

                {/*//className="container">*/}
                <Menu/>
                    {/*<h1>Name</h1>*/}
                    <br/>
                <br/><br/><br/>
                <table className="uk-table uk-table-hover uk-table-divider" style={{position: "relative"}}>
                    {/*<table className="table" style={{position: "relative"}}>*/}
                        <thead>
                        <tr>
                            <th>Image</th>
                            <th>Name</th>
                            <th>Description</th>
                            <th>ABV</th>
                            <th>EBC</th>
                            <th>AdminButton</th>
                        </tr>
                        </thead>
                        <tbody>
                        {
                            this.state.beers.length ?
                                this.state.beers.map(item => {
                                    return (
                                        <Beer data={item} btntext="Supprimer" delete={(id) => this.deleteBeer(id)}/>
                                    )
                                }) : null

                        }
                        </tbody>
                    </table>
                </div>
        )
    }
}

export default Beers;