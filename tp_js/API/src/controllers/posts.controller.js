import Post from "../models/Post";

class PostController {

    /**
     * Create Article into Database
     * @param {Request} req
     * @param {Response} res
     */
    static async create(req, res) {
        let status = 200;
        let body = {};

        try {

            let post = await Post.create({
                title: req.body.title,
                content: req.body.content,
                userId: req.body.userId
            });

            body = {
                post,
                'message': 'Post created'
            };

        } catch (error) {
            status = 500;
            body = {'message': error.message};
        }
        return res.status(status).json(body);
    }

    static async list(req, res) {
        let status = 200;
        let body = {};

        try {
            let post = await Post.find().populate('userId');

            body = {
                post,
                'message': 'Posts list'
            };
        } catch (error) {
            status = 500;
            body = {'message': error.message};
        }
        return res.status(status).json(body);
    }
    static async details(req, res){
        let status = 200;
        let body = {};

        try {
            let id = req.params.id;
            let post = await Post.findByID(id).populate('userId');

            body = {
                post,
                'message': 'Posts details'
            };
        } catch (error) {
            status = 500;
            body = {'message': error.message};
        }
        return res.status(status).json(body);
    }
    static async delete(req, res){
        let status = 200;
        let body = {};

        try {
            await Post.remove({_id: req.params.id});
            body = {
                'message': 'Post delete'
            };
        } catch (error) {
            status = 500;
            body = {'message': error.message};
        }
        return res.status(status).json(body);
    }
    static async update(req, res){
        let status = 200;
        let body = {};

        try {
            // methode 1
            let post = await Post.findById(req.params.id);
            Object.assign(post, req.body);
            await post.save();

            body = {
                post,
                'message': 'Posts update'
            };
        } catch (error) {
            status = 500;
            body = {'message': error.message};
        }
        return res.status(status).json(body);
    }
}

export default PostController;